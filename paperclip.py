#!/usr/bin/env python

#### paperclip.py
#### Plotting and Analyzing Proteins Employing Rosetta CLI with PAPERCLIP
#### by Maria Szegedy, 2019 Jun
#### for Khare Lab at Rutgers University

'''This is an interactive suite for generating a variety of plots based on data
obtained from Rosetta and MPRE (mszpyrosettaextension). Mostly the plots
revolve around data that I've had to collect for research, or personal
interest. Most plotting commands are based on Matlab commands.
'''

### Imports
## Python Standard Library
# pylint: disable=import-error
import re
import copy
import math
import fractions
import itertools, functools
from functools import reduce
import csv
import hashlib
import os, io, argparse
import subprocess
import cmd, shlex
import sys, traceback
import ast
## Other stuff
# Mahmoud's boltons and other utilities
from boltons import funcutils
from boltons import setutils
# Decorators
from decorator import decorator
import timeout_decorator
# Multithreading
from mpi4py import MPI
import dill
# Plotting
import matplotlib
matplotlib.use('Agg') # otherwise lack of display breaks it
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import seaborn as sns
sns.set()
import imageio
# Scientific computation
import numpy as np
import bottleneck
import pyrosetta as pr
import mszpyrosettaextension as mpre
import pytraj as pt
import zaluski

### Constants and messing around with libraries

DEBUG = True

STDOUT = sys.stdout
MPICOMM   = MPI.COMM_WORLD
MPIRANK   = MPICOMM.Get_rank()
MPISIZE   = MPICOMM.Get_size()
MPISTATUS = MPI.Status()
PYROSETTA_ENV = None
ROSETTA_RMSD_TYPES = ['gdtsc',
                      'CA_rmsd',
                      'CA_gdtmm',
                      'bb_rmsd',
                      'bb_rmsd_including_O',
                      'all_atom_rmsd',
                      'nbr_atom_rmsd']

### Decorators
## Decorators that work with PYROSETTA_ENV

@decorator
def needs_pr_init(f, *args, **kwargs):
    '''Makes sure PyRosetta gets initialized before f is called.'''
    global PYROSETTA_ENV
    PYROSETTA_ENV.init()
    return f(*args, **kwargs)

@decorator
def needs_pr_scorefxn(f, *args, **kwargs):
    '''Makes sure the scorefxn exists before f is called. (In order for the
    scorefxn to exist, pr.init() needs to have been called, so @needs_pr_init
    is unnecessary in front of this.)'''
    global PYROSETTA_ENV
    if PYROSETTA_ENV.scorefxn is None:
        print('Scorefxn not initialized yet. Initializing from defaults...')
        PYROSETTA_ENV.set_scorefxn()
    return f(*args, **kwargs)

## Decorators that work with FileDataBuffer

def uses_pr_env(f):
    '''Sets an attribute on the function that tells FileDataBuffer to include a
    hash of the PyRosetta env as a storage key for the output of this function.
    Useless for functions that aren't calculate_ methods in FileDataBuffer.'''
    def additional_hashes():
        global PYROSETTA_ENV
        return (PYROSETTA_ENV.hash(),)
    f.additional_hashes = additional_hashes
    return f

## Decorators that add functionality when run by users

def times_out(f):
    '''Makes a function time out after it hits self.timelimit.'''
    @funcutils.wraps(f)
    def decorated(self_, *args, **kwargs):
        if self_.timelimit:
            try:
                return timeout_decorator \
                    .timeout(self_.timelimit)(f)(self_, *args, **kwargs)
            except timeout_decorator.TimeoutError:
                print('Timed out.')
        else:
            return f(self_, *args, **kwargs)
    return decorated

def continuous(f):
    '''Makes a function in OurCmdLine repeat forever when continuous mode is
    enabled, and decorates it with times_out.'''
    @funcutils.wraps(f)
    @times_out
    def decorated(self_, *args, **kwargs):
        if self_.settings['continuous_mode']:
            while True:
                f(self_, *args, **kwargs)
        else:
            return f(self_, *args, **kwargs)
    return decorated

def pure_plotting(f):
    '''Wraps "pure plotting" cmd commands that shouldn't be executed unless
    plotting is enabled.'''
    @funcutils.wraps(f)
    def decorated(self_, *args, **kwargs):
        if self_.settings['plotting']:
            return f(self_, *args, **kwargs)
        else:
            return
    return decorated

def single_threaded(f):
    '''Wraps single-threaded commands that shouldn't be executed except by the
    master thread.'''
    @funcutils.wraps(f)
    def decorated(self_, *args, **kwargs):
        if MPIRANK == 0:
            return f(self_, *args, **kwargs)
        else:
            return
    return decorated

### Useful functions
## vanilla stuff

def DEBUG_OUT(*args, **kwargs):
    if DEBUG:
        print('DEBUG: ', end='')
        kwargs['flush'] = True
        print(*args, **kwargs)
    try:
        return(args[0])
    except IndexError:
        pass

def get_filenames_from_dir_with_extension(dir_path, extension,
                                          strip_extensions_p=False):
    '''Returns a list of files from a directory with the path stripped, and
    optionally the extension stripped as well.'''
    path_list = str(subprocess.check_output('ls '+os.path.join(dir_path,
                                                               '*'+extension),
                                            shell=True)).split('\\n')
    # Doesn't hurt to validate twice
    stripper = None
    if strip_extensions_p:
        stripper = re.compile(r'[^\\]/([^/]+)' + re.escape(extension) + r'$')
    else:
        stripper = re.compile(r'[^\\]/([^/]+' + re.escape(extension) + r')$')
    # Premature optimization is the root of all evil, but who wants to run
    # the same regex twice?
    return [m.group(1) \
            for m \
            in [stripper.search(path) for path in path_list] \
            if m is not None]

def process_limits(limits):
    '''Processes a limits arg for the cmd object. Returns a list of
    [lower_limit, upper_limit]. Sadly still has to be wrapped in a try/catch on
    the outside.'''
    return [(None if val.lower() in ('same', 'unbound', 'none') \
             else float(val)) \
            for val in limits]

def process_params(params):
    '''Process params list so that .params is appended if a path doesn't end
    with it.'''
    if params:
        return [(param if param.endswith('.params') \
                 else param+'.params') \
                for param in params]
    else:
        return None

## data manipulation

def least_common_multiple(*l):
    '''Least common multiple of some numbers.'''
    if len(l) == 0:
        raise ValueError('Least common multiple is not defined for sets of '
                         'zero numbers.')
    return reduce(lambda a,b: abs(a*b) / fractions.gcd(a,b) if a and b else 0,
                  l)

def remove_ndarray_diagonals(m, n_removed):
    '''Remove diagonals from the center of and flatten a matrix.'''
    return np.hstack(
        [np.hstack(m[i][i+n_removed//2+1:] \
                    for i in range(m.shape[0])),
            np.hstack(m[i][:max(i-n_removed//2,0)] \
                    for i in range(m.shape[0]))])

def trim_outliers_left(l, threshold=2.5):
    '''Takes a list of numbers, where the left hand side is assumed to be much
    further away from the global average than the right hand side. Returns a
    version of the list where the right-most 1/e-th of the numbers is first
    unconditionally included, then the left hand limit of the slice is walked
    leftwards until a number is encountered that is more than 'threshold'
    standard deviations from the mean of the initial 1/e-th.'''
    start_index = int(len(l)*0.632)
    start_slice = l[start_index:]
    mean = np.mean(start_slice)
    std  = np.std(l)
    scaled_threshold = threshold*std
    new_start_index = start_index
    while new_start_index > 0:
        new_start_index -= 1
        if not math.abs(l[new_start_index] - mean) <= scaled_threshold:
            new_start_index += 1
            break
    return l[new_start_index:]

## Input processing functions for OurCmdLine

def process_subgrid(arg):
    arg = ''.join((c for c in arg if c != ' '))
    matcher = re.compile(r'\(?\((\d+),(\d+)\),?'     # first coordinate
                            r'\((\d+),(\d+)\),?\)?') # second coordinate
    matches = matcher.match(arg)
    if matches is not None and None not in [matches.group(n) for n in range(5)]:
        return ((int(matches.group(1)), int(matches.group(2))),
                (int(matches.group(3)), int(matches.group(4))))
    else:
        raise ValidationError

def process_ticks(arg):
    '''Given the input string arg, return a tuple of a tuple of tick labels and
    an ndarray of tick indices that it describes. It's possible that it does not
    describe any tick indices, in which case that part of the returning tuple
    is left as none. The acceptable formats for arg are:
        "'A A' 'B B' 'C C'"           -> (('A A', 'B B', 'C C'), None)
        "A\ A B\ B C\ C"              -> (('A A', 'B B', 'C C'), None)
        "('A', 1), ('B', 2), ('C', 3) -> (('A', 'B', 'C'), (1, 2, 3))"'''
    tick_indices = None
    tick_labels  = None
    try:
        parsed = ast.literal_eval(arg)
        try:
            tick_labels, tick_indices = zip(parsed)
            tick_indices = np.array(tick_indices)
        except:
            raise ValidationError
    except SyntaxError:
        try:
            parsed = ast.literal_eval("'%s'," % "','".join(shlex.split(arg)))
            tick_indices = np.arange(len(parsed))
            tick_labels = parsed
        except:
            raise ValidationError
    return (tick_indices, tick_labels)

### Housekeeping classes

class ValidationError(Exception):
    '''Exception raised by functions that validate input, when validation
    fails.'''
    pass

class PyRosettaEnv():
    '''Stores stuff relating to PyRosetta, like whether pr.init() has been
    called.'''
    def __init__(self):
        self.initp = False
        self.scorefxn = None
    def init(self):
        if not self.initp:
            print('PyRosetta not initialized yet. Initializing...')
            pr.init()
            self.initp = True
    @needs_pr_init
    def set_scorefxn(self, name=None, patch=None):
        '''Sets the scorefxn and optionally applies a patch. Defaults to
        get_fa_scorefxn() with no arguments.'''
        if name is None:
            self.scorefxn = pr.get_fa_scorefxn()
        else:
            self.scorefxn = pr.create_score_function(name)
        if patch:
            self.scorefxn.apply_patch_from_file(patch)
    @property
    @needs_pr_scorefxn
    def hash(self):
        '''Gets a hash of the properties of the current env.'''
        hash_fun = hashlib.md5()
        weights = self.scorefxn.weights()
        hash_fun.update(weights.weighted_string_of(weights).encode())
        return hash_fun.hexdigest()

class PDBDataBuffer(zaluski.FileDataBuffer):
    ## Calculating stuff
    # Each calculate_<whatever> also implicity creates a get_<whatever>, which
    # is just the same thing but with all the buffer/caching magic attached,
    # and with stream args replaced with path args. It also creates a
    # gather_<whatever>, which outwardly looks like get_<whatever> operating on
    # a list instead of a single filename, but is actually concurrently
    # controlled if there are multiple processors available. It is possible to
    # write import_ and export_ methods to convert the results of a calculate_
    # method from and to a JSON-storeable format. These must take only one
    # argument each (besides self), and must come in pairs.
    @uses_pr_env
    @needs_pr_scorefxn
    def calculate_score(self, pdb_path, params=None, cst_path=None):
        '''Calculates the score of a protein from a stream of its PDB file.
        scorefxn_hash is not used inside the calculation, but is used for
        indexing into the buffer.
        '''
        global PYROSETTA_ENV
        pose = mpre.pose_from_file(pdb_path, params=params)
        if cst_path is not None:
            mpre.add_constraints_from_file(pose, cst_path)
        return PYROSETTA_ENV.scorefxn(pose)
    @needs_pr_init
    def calculate_rmsd(self, lhs_path, rhs_path, rmsd_type, params=None):
        '''Calculates the RMSD of two proteins from each other and stores
        it, without assuming commutativity.'''
        lhs_pose = mpre.pose_from_file(lhs_path, params=params)
        rhs_pose = mpre.pose_from_file(rhs_path, params=params)
        return getattr(pr.rosetta.core.scoring, rmsd_type)(lhs_pose, rhs_pose)
    @zaluski.returns_ndarray
    @needs_pr_init
    def calculate_neighbors(self, pdb_path, params=None, coarsep=False,
                            bound=None):
        '''Calculates the residue neighborhood matrix of a protein based on the
        provided contents of its PDB file.'''
        pose = mpre.pose_from_file(pdb_path, params=params)
        result = []
        n_residues = pose.size()
        for i in range(1,n_residues+1):
            result.append([])
            for j in range(1,n_residues+1):
                if i > j:
                    result[-1].append(result[j-1][i-1])
                else:
                    result[-1].append(
                        int(mpre.res_neighbors_p(pose,
                                                 i, j,
                                                 coarsep=coarsep,
                                                 bound=bound)))
        return np.array(result)
    def calculate_residues(self, prmtop_path, mask=''):
        '''Retrieves a list of residue names and numbers from a .prmtop file.
        Also apparently works for .pdb, .mol2, .psf, .cif, Gromacs topology,
        .sdf, and .xyz files.'''
        top = pt.load_topology(prmtop_path)
        return [(r.original_resid, r.name) \
                for r in top[mask].residues]
    @zaluski.returns_ndarray
    def calculate_traj_rmsd(self, mdcrd_path, prmtop_path,
                            mask='!:WAT,Na+,Cl-', reference_path=None):
        '''Calculates the RMSD over time of a trajectory, compensating for
        rotation, from the first frame. Returns a one-dimensional ndarray with
        one RMSD for each frame.'''
        if reference_path is not None:
            traj = pt.iterload([reference_path, mdcrd_path], prmtop_path)
        else:
            traj = pt.iterload(mdcrd_path, prmtop_path)
        traj.autoimage()
        ret = np.array(pt.rmsd(traj, ref=0, mask=mask))
        if reference_path is not None:
            return ret[1:]
        else:
            return ret
    @zaluski.returns_ndarray
    def calculate_traj_residue_rmsd(self, mdcrd_path, prmtop_path,
                                    mask='!:WAT,Na+,Cl-', reference_path=None):
        '''Calculates the RMSD over time of each residue in a trajectory,
        compensating for rotation, from the first frame. Returns a 2D ndarray
        with one row for each residue and one column for each frame.'''
        residues, _ = zip(*self.get_residues(prmtop_path, mask=mask))
        return np.array(self.gather_traj_rmsd(mdcrd_path, prmtop_path,
                                              argi='mask',
                                              mask=[':'+str(r) \
                                                    for r in residues],
                                              reference_path=reference_path))
    @zaluski.non_cacheable
    @zaluski.returns_ndarray # doesn't do anything if @non_cacheable is there
    def calculate_traj_pairwise_distances(self, mdcrd_path, prmtop_path,
                                          mask_1='@CA', mask_2='@CA'):
        '''Calculates a pairwise distance matrix for two atom masks over a
        trajectory. Returns one matrix for each frame.'''
        traj = pt.iterload(mdcrd_path, prmtop_path)
        return pt.pairwise_distance(traj,
                                    mask_1=mask_1,
                                    mask_2=mask_2)[0]

### Helper classes for working with matplotlib (bleh)
class PlotWrapper:
    '''Wraps a static plot that occupies a single subplot index.'''
    def __init__(self, plot_type, *plot_args, **plot_kwargs):
        #### THIS PROBABLY USES MASSIVE AMOUNTS OF RAM PLEASE FIX ####
        ####              (or just buy more ram lol)              ####
        # string of Axes method name that produces the plot:
        self.plot_type   = plot_type
        # arguments to pass to plot method:
        self.plot_args   = plot_args
        self.plot_kwargs = plot_kwargs
        # plot limits; should be two-element tuples of upper and lower bounds:
        self.xlim = None
        self.ylim = None
        # axes labels:
        self.xlabel = None
        self.ylabel = None
        # tick labels and indices (None means use default values):
        self.xtick_indices = None
        self.xtick_labels  = None
        self.ytick_indices = None
        self.ytick_labels  = None
        # tick label rotations ('horizontal', 'vertical', or degrees):
        self.xtick_rotation = None
        self.ytick_rotation = None
        # misc:
        self.gridp = None       # grid lines, t/f?
        self.title = None       # plot title
        self.tick_params = None # args to pass to plt.tick_params() before plot
    def set_plot(self, plot_type, *plot_args, **plot_kwargs):
        '''Sets .plot_type, .plot_args, and .plot_kwargs. Useful when you want
        to modify the PlotWrapper's base attributes after the fact, as in
        AnimatedAxesWrapper dummy PlotWrappers.'''
        self.plot_type   = plot_type
        self.plot_args   = plot_args
        self.plot_kwargs = plot_kwargs
    def plot(self, ax):
        ret = getattr(ax, self.plot_type)(*self.plot_args, **self.plot_kwargs)
        if self.xlim is not None:
            if self.xlim[0] is not None:
                ax.set_xlim(left=self.xlim[0])
            if self.xlim[1] is not None:
                ax.set_xlim(right=self.xlim[1])
        if self.ylim is not None:
            if self.ylim[0] is not None:
                ax.set_ylim(bottom=self.ylim[0])
            if self.ylim[1] is not None:
                ax.set_ylim(top=self.ylim[1])
        if self.xlabel is not None:
            ax.set_xlabel(self.xlabel)
        if self.ylabel is not None:
            ax.set_ylabel(self.ylabel)
        if self.xtick_indices is not None:
            ax.set_xticks(self.xtick_indices)
        if self.xtick_labels is not None:
            ax.set_xticklabels(self.xtick_labels)
        if self.ytick_indices is not None:
            ax.set_yticks(self.ytick_indices)
        if self.ytick_labels is not None:
            ax.set_yticklabels(self.ytick_labels)
        if self.xtick_rotation is not None:
            for text in ax.get_xticklabels():
                text.set_rotation(self.xtick_rotation)
        if self.ytick_rotation is not None:
            for text in ax.get_yticklabels():
                text.set_rotation(self.ytick_rotation)
        if self.gridp is not None:
            ax.grid(self.gridp)
        if self.title is not None:
            ax.set_title(self.title)
        if self.tick_params is not None:
            ax.tick_params(**self.tick_params)
        return ret

class AnimatedAxesWrapper:
    '''Wraps an animation of a single axes object.'''
    def __init__(self, init_f, update_f):
        # called before the animation is run, and takes an Axes object as its
        # first argument, and self.env as its second argument
        self.init_f = init_f
        self.init_args   = []
        self.init_kwargs = {}
        # update_f is called during every frame of the animation, and takes an
        # Axes object as its first argument, the frame number as its second
        # argument, and self.env as its third. It should return False when it
        # actually did something, and True when it didn't. It should also
        # always return True at some point; otherwise, the animation will run
        # forever.
        self.update_f = update_f
        self.update_args   = []
        self.update_kwargs = {}
        self.current_frame = None
        # Environment containing any persistent objects for animation; will
        # contain at least a PlotWrapper, under the keyword 'pw' (easily
        #  retrieved from the outside with the property method .pw). It's the
        # job of init_f to populate it properly.
        self.env = {'pw': PlotWrapper(None, None)}
        # Environment containing any Artists whose values change during the
        # animation, grouped here so that the surrounding MatplotlibWrapper
        # knows which ones to blit onto the canvas. Every animation should
        # contain at least one drawable; otherwise, it is invisible.
        self.drawables = {}
    @property
    def pw(self):
        '''The main PlotWrapper object of the animation. Returns None if there
        isn't any.'''
        return self.env.get('pw')
    @pw.setter
    def pw(self, value):
        self.env['pw'] = value
    def plot(self, ax):
        if self.pw is not None:
            return self.pw.plot(ax)
    def set_init_args(self, *args, **kwargs):
        self.init_args   = args
        self.init_kwargs = kwargs
    def set_update_args(self, *args, **kwargs):
        self.update_args   = args
        self.update_kwargs = kwargs
    def init(self, ax):
        '''Called to initialize the Axes that this animation is responsible for.
        Should not draw anything that shouldn't be in every frame, since
        whatever this draws will be used as the background for every subsequent
        frame.'''
        self.current_frame = None
        ret = self.init_f(ax,
                          self.env,
                          self.drawables,
                          *self.init_args,
                          **self.init_kwargs)
        for drawable in self.drawables.values():
            drawable.set_visible(False)
        return ret
    def update(self, ax):
        if self.current_frame is None:
            frame = 0
            for drawable in self.drawables.values():
                drawable.set_visible(True)
        else:
            frame = self.current_frame + 1
        # update_f is supposed to return a falsy value (ideally False) if
        # something actually happened during the update, and a truthy value
        # (ideally True) if nothing happened and it's finished iterating. If it
        # returns a truthy value, the animation gets reset, so make sure it
        # only returns a truthy value when it actually has nothing else to do.
        endedp = self.update_f(ax,
                               frame,
                               self.env,
                               self.drawables,
                               *self.update_args,
                               **self.update_kwargs)
        if endedp:
            self.init(ax)
        else:
            self.current_frame = frame
        return self.current_frame
    def draw(self, ax):
        for drawable in self.drawables.values():
            ax.draw_artist(drawable)

class MatplotlibBuffer:
    def __init__(self):
        ## pre-init, post-init commands
        # commands to perform before plots and animation inits are run,
        # encapsulated in functools.partial objects taking no args:
        self.pre_commands = []
        # commands to perform after plots and animation inits are run,
        # encapsulated in functools.partial objects taking no args:
        self.post_commands = []

        ## GridSpec emulation stuff
        # (# rows, # cols) for grid in GridSpec; controlled through property
        # grid_size, never invalidated
        self._grid_size = (1, 1)
        # matplotlib gridspec object representing grid on which axes objects
        # are placed; controlled through property gridspec, invalidated when
        # grid_size is changed
        self._gridspec = None
        # Current index at which new schemes are saved, which is the two sets
        # of grid coordinates that will be used for the GridSpec position of
        # the axes at which the scheme will be made. Coordinates are
        # slice-style, going in-between actual grid locations. So an m by n
        # grid will have y coords running from 0 to m, and x coords running
        # from 0 to n.
        self.subgrid = ((0,0), (1,1))

        ## plotting and animation stuff
        # tuple containing canvas size in inches, as (w, h)
        self.canvas_size = None
        # dict containing all plots and animations (collectively called
        # "schemes"); should only be added with .add_plot() and
        # .add_animation(), which will both use self.subgrid as the key
        self.schemes = {}
        # (subgrid, index) index pairs for all animations saved
        self.animation_indices = setutils.IndexedSet()
        # bool that tracks whether .init_figure_bg() has been called yet
        self.bg_initialized_p = False
        # current "global" frame number; starts out as None until
        # .init_figure_fg() is called for the first time
        self.current_frame = None
    ## Properties
    @property
    def grid_size(self):
        return self._grid_size
    @grid_size.setter
    def grid_size(self, value):
        # invalidate gridspec cache
        self._gridspec = None
        # clear axes from figure
        fig = plt.gcf()
        for ax in copy.copy(fig.axes):
            fig.delaxes(ax)
        # save value
        self._grid_size = value
    @property
    def gridspec(self):
        if self._gridspec is not None:
            return self._gridspec
        if self.grid_size == (1,1):
            return None
        self._gridspec = matplotlib.gridspec.GridSpec(*self.grid_size)
        return self._gridspec
    @property
    def current_axes(self):
        if self.grid_size == (1,1):
            ax = plt.gca()
        else:
            return self.get_axes_from_subgrid(self.subgrid)
    @property
    def current_scheme_list(self):
        return self.schemes.setdefault(self.subgrid, [])
    @current_scheme_list.setter
    def current_scheme_list(self, value):
        self.schemes[self.subgrid] = value
    @property
    def current_scheme(self):
        try:
            return self.current_scheme_list[-1]
        except IndexError:
            return None
    @current_scheme.setter
    def current_scheme(self, value):
        # "let it fail"; you can always catch the error or check beforehand to
        # see whether current_scheme is None
        self.current_scheme_list[-1] = value
    @property
    def current_plot(self):
        if isinstance(self.current_scheme, PlotWrapper):
            return self.current_scheme
        elif isinstance(self.current_scheme, AnimatedAxesWrapper):
            return self.current_scheme.pw
        else:
            raise TypeError('Current scheme is neither a PlotWrapper nor an '
                            'AnimatedAxesWrapper.')
    @current_plot.setter
    def current_plot(self, value):
        # this method should fail if and only if one of the following is true:
        # - assigning to current_scheme also fails
        # - current_scheme is neither a PlotWrapper nor an AnimatedAxesWrapper
        if isinstance(self.current_scheme, PlotWrapper):
            self.current_scheme = value
        elif isinstance(self.current_scheme, AnimatedAxesWrapper):
            self.current_scheme.pw = value
        else:
            raise TypeError('Current scheme is neither a PlotWrapper nor an '
                            'AnimatedAxesWrapper.')
    ## Interaction with self.pre_commands and self.post_commands
    def add_pre_command(self, f, *args, **kwargs):
        self.pre_commands.append(functools.partial(f, *args, **kwargs))
    def add_post_command(self, f, *args, **kwargs):
        self.post_commands.append(functools.partial(f, *args, **kwargs))
    ## Interaction with self.schemes
    def add_scheme(self, scheme):
        if isinstance(scheme, AnimatedAxesWrapper):
            self.animation_indices.add((self.subgrid,
                                        len(self.current_scheme_list)))
            self._total_frames = None
        self.current_scheme_list.append(scheme)
    def add_plot(self, plot_command, *plot_args, **plot_kwargs):
        self.add_scheme(PlotWrapper(plot_command, *plot_args, **plot_kwargs))
    def add_animation(self, init_f, update_f):
        self.add_scheme(AnimatedAxesWrapper(init_f, update_f))
    ## Interaction with matplotlib :(
    # Base interactions
    def clear_matplotlib(self):
        '''Should reset matplotlib to its state at the beginning of the
        program, as well as this object's member variables that keep track of
        it.'''
        # surprisingly, del is not the correct thing to use here, since there
        # are functions that specifically check whether these are None. that
        # said, if garbage collection is happening too slowly, you could always
        # del and /then/ set it to None, i guess
        self._gridspec        = None
        self.bg_initialized_p = False
        self.current_frame    = None
        plt.cla()
        plt.clf()
    def get_axes_from_subgrid(self, subgrid):
        if self.grid_size == (1,1) and subgrid == ((0,0), (1,1)):
            return plt.gca()
        (tx, ty), (bx, by) = subgrid
        return plt.subplot(self.gridspec[ty:by, tx:bx])
    def init_figure_bg(self):
        '''Clear and initialize matplotlib. This only initializes the "static"
        elements, i.e. PlotWrappers and the backgrounds of animations.'''
        self.clear_matplotlib()
        for command in self.pre_commands:
            command()
        if self.canvas_size is not None:
            plt.gcf().set_size_inches(*self.canvas_size)
        gs = self.gridspec
        if gs is None:
            ax = plt.gca()
        else:
            ax = None
        for subgrid, scheme_list in self.schemes.items():
            if gs is not None:
                ax = self.get_axes_from_subgrid(subgrid)
            for scheme in scheme_list:
                if isinstance(scheme, PlotWrapper):
                    scheme.plot(ax)
                elif isinstance(scheme, AnimatedAxesWrapper):
                    scheme.init(ax)
        for command in self.post_commands:
            command()
        self.bg_initialized_p = True
    def init_figure_fg(self):
        '''Initialize the foregrounds of each animation (the drawables), setting
        the .current_frame of both this object and each animation to 0 (from
        None). Does nothing when it finds .current_frame is None, both globally
        and per-animation.'''
        assert self.bg_initialized_p
        if self.current_frame is None:
            for subgrid, index in self.animation_indices:
                ax = self.get_axes_from_subgrid(subgrid)
                scheme = self.schemes[subgrid][index]
                if scheme.current_frame is None:
                    scheme.update(ax)
        self.current_frame = 0
    def run_animations(self, n_frames):
        '''Execute animations for n_frames frames. If there was ever a frame
        advance where every animation reset itself, return the number of frame
        advances that occurred after the first such event plus 1. Otherwise,
        return None.'''
        assert self.current_frame is not None, \
               'Buffer must be initialized first with .init_figure_bg() and ' \
               '.init_figure_fg().'
        for _ in range(n_frames):
            # list of frame numbers that each animation was left at
            leaving_frames = []
            # frames it's been since leaving_frames was first all 0s
            frames_since_first_sync = None
            for subgrid, index in self.animation_indices:
                DEBUG_OUT('Updating animation at subgrid', subgrid, 'and index',
                          index)
                ax = self.get_axes_from_subgrid(subgrid)
                scheme = self.schemes[subgrid][index]
                # .update() returns the Animated
                leaving_frames.append(scheme.update(ax))
            if frames_since_first_sync is None:
                if False not in [x == 0 for x in leaving_frames]:
                    frames_since_first_sync = 1
            else:
                frames_since_first_sync += 1
            self.current_frame += 1
        return frames_since_first_sync
    # Higher-order (read: front-end) interactions
    def clear(self):
        self.__init__()
        self.clear_matplotlib()
    def set_current_frame(self, value):
        '''Walks animations forwards until self.current_frame == value. If
        self.current_frame > value, it resets the system.'''
        resetp = isinstance(self.current_frame, int) and \
                 (self.current_frame > value or value == 0)
        if not self.bg_initialized_p or resetp:
            self.init_figure_bg()
        if self.current_frame is None or resetp:
            self.init_figure_fg()
        if self.current_frame == value:
            return
        self.run_animations(value - self.current_frame)
    def save_static(self, path, format=None, frame=0):
        # Yes I know format is a built-in function. Blame matplotlib.
        self.set_current_frame(frame)
        plt.savefig(path, format=format)
    def save_animation(self, path, format=None, fps=30, start_frame=0):
        DEBUG_OUT('Saving animation at '+path)
        # all formats are allcaps, but i feel more comfortable being able to
        # give lowercase formats
        if hasattr(format, 'upper'):
            format = format.upper()
        # source of pixels:
        canvas = plt.gcf().canvas
        # destination of pixels:
        writer = imageio.get_writer(path, format=format, mode='I', fps=fps)

        # get background image:
        self.init_figure_bg()
        canvas.draw()
        backgrounds = {subgrid: canvas.copy_from_bbox(
                                           self.get_axes_from_subgrid(subgrid) \
                                               .bbox) \
                       for subgrid in self.schemes}
        # the actual writing:
        self.set_current_frame(start_frame)
        DEBUG_FRAME_COUNT = start_frame
        while True:
            DEBUG_OUT('Saving frame %d' % DEBUG_FRAME_COUNT)
            DEBUG_OUT('Drawing canvas...')
            for subgrid, background in backgrounds.items():
                canvas.restore_region(background)
            for subgrid, index in self.animation_indices:
                ax = self.get_axes_from_subgrid(subgrid)
                self.schemes[subgrid][index].draw(ax)
                canvas.blit(ax.bbox)
            DEBUG_OUT('Appending data to writer...')
            writer.append_data(np.array(canvas.renderer._renderer))
            DEBUG_OUT('Running animations...')
            result = self.run_animations(1)
            # result should be None if the frame advance didn't make every
            # animation reset, 1 if it did; see documentation of
            # .run_animations()
            if result is not None:
                break
            DEBUG_FRAME_COUNT += 1
        writer.close()

### Main class

class OurCmdLine(cmd.Cmd):
    '''Singleton class for our interactive CLI.'''
    ## Built-in vars
    intro = 'Welcome to PAPERCLIP. Type help or ? to list commands.'
    prompt = '* '
    ## Our vars (don't wanna mess with __init__)
    cmdfile = None
    settings = {'calculation': True,
                'caching': True,
                'debug': DEBUG,
                'plotting': MPIRANK == 0,
                'continuous_mode': False}
    timelimit = 0
    ## The three buffers:
    mtpl_buffer = MatplotlibBuffer() # contains queued plots and animations
    data_buffer = PDBDataBuffer(filename='.paperclip_cache',
                                version=1) # contains computed data about files
    text_buffer = io.StringIO()      # contains text output, formatted as a TSV

    ## Housekeeping
    def do_quit(self, arg):
        '''Stop recording and exit:  quit'''
        self.close()
        return True
    def do_bye(self, arg):
        '''Stop recording and exit:  bye'''
        return self.do_quit(arg)
    def do_EOF(self, arg):
        '''Stop recording and exit:  EOF  |  ^D'''
        return self.do_quit(arg)
    def do_exit(self, arg):
        '''Stop recording and exit:  exit'''
        return self.do_quit(arg)
    def emptyline(self):
        pass
    @single_threaded
    def do_echo(self, arg):
        '''Print something:  echo Hello world!'''
        print(arg.encode('utf-8'))

    ## Making argparse-generated docs work
    def preloop(self):
        # Most of the following was copied from the original cmd.py with very
        # little modification; I hereby absolve myself of any responsibility
        # regarding it
        names = self.get_names()
        cmds_undoc = []
        help = set()
        for name in names:
            if name[:5] == 'help_':
                help.add(name[5:])
        names.sort()
        # There can be duplicates if routines overridden
        prevname = ''
        for name in names:
            if name[:3] == 'do_':
                if name == prevname:
                    continue
                prevname = name
                cmd=name[3:]
                if not ((cmd in help) or (getattr(self, name).__doc__)):
                    cmds_undoc.append(cmd)
        # This is my code. It runs the undocumented flags with the '-h' flag
        # and output silenced, which allows the command's __doc__ to be
        # overwritten but not the rest of the script to be executed (since in
        # that case, the parse_args() line throws a SystemExit exception, which
        # I manually catch and turn into a return).
        with open(os.devnull, 'w') as DEVNULL:
            sys.stdout = DEVNULL
            for cmd in cmds_undoc:
                self.onecmd(cmd+' -h')
        sys.stdout = STDOUT

    def cmdloop(self, intro=None):
        '''Repeatedly issue a prompt, accept input, parse an initial prefix
        off the received input, and dispatch to action methods, passing them
        the remainder of the line as argument.
        This is almost exactly the original version, except for that the intro
        is only printed if the thread is the MPI "master".'''
        self.preloop()
        if self.use_rawinput and self.completekey:
            try:
                import readline
                self.old_completer = readline.get_completer()
                readline.set_completer(self.complete)
                readline.parse_and_bind(self.completekey+": complete")
            except ImportError:
                pass
        try:
            if intro is not None:
                self.intro = intro
            if self.intro and MPIRANK == 0:
                self.stdout.write(str(self.intro)+"\n")
            stop = None
            while not stop:
                if self.cmdqueue:
                    line = self.cmdqueue.pop(0)
                else:
                    if self.use_rawinput:
                        try:
                            line = input(self.prompt)
                        except EOFError:
                            line = 'EOF'
                    else:
                        self.stdout.write(self.prompt)
                        self.stdout.flush()
                        line = self.stdin.readline()
                        if not len(line):
                            line = 'EOF'
                        else:
                            line = line.rstrip('\r\n')
                line = self.precmd(line)
                stop = self.onecmd(line)
                stop = self.postcmd(stop, line)
            self.postloop()
        finally:
            if self.use_rawinput and self.completekey:
                try:
                    import readline
                    readline.set_completer(self.old_completer)
                except ImportError:
                    pass


    ## Parsing
    def get_arg_position(self, text, line):
        '''For completion; gets index of current positional argument (returns 1 for
        first arg, 2 for second arg, etc.).'''
        return len(line.split()) - (text != '')
    def split_cst_path(self, path):
        '''Splits off a colon-separated cst file from the end of a path.'''
        split = path.split(':')
        if len(split) > 1:
            return (':'.join(split[:-1]), split[-1])
        else:
            return (path, None)

    ## Recording and playing back commands
    @single_threaded
    def do_record(self, arg):
        '''Save future commands to filename:  record plot.cmd'''
        self.cmdfile = open(arg, 'w')
    def do_playback(self, arg):
        '''Play back commands from a file:  playback plot.cmd'''
        self.close()
        with open(arg) as f:
            self.cmdqueue.extend(f.read().splitlines())
    def precmd(self, line):
        if self.cmdfile and 'playback' not in line:
            print(line, file=self.cmdfile)
        return line
    def close(self):
        if self.cmdfile:
            self.cmdfile.close()
            self.cmdfile = None

    ## Shell stuff
    def do_shell(self, arg):
        '''Call a shell command:  shell echo 'Hello'  |  !echo 'Hello' '''
        os.system(arg)
    def do_cd(self, arg):
        '''Change the current working directory:  cd dir'''
        try:
            os.chdir(arg)
        except FileNotFoundError:
            print('No such file or directory: ' + arg)
    def do_mv(self, arg):
        '''Call the shell command mv:  mv a b'''
        self.do_shell('mv ' + arg)
    def do_rm(self, arg):
        '''Call the shell command rm:  rm a'''
        self.do_shell('rm ' + arg)
    def do_ls(self, arg):
        '''Call the shell command ls:  ls ..'''
        self.do_shell('ls ' + arg)
    @single_threaded
    def do_pwd(self, arg):
        '''Get the current working directory:  pwd'''
        print(os.getcwd())

    ## Settings
    def do_get_settings(self, arg):
        '''Print settings of current session:  get_settings'''
        for key, value in self.settings.items():
            transformed_value = 'yes' if value == True else \
                                'no'  if value == False else value
            print('{0:<20}{1:>8}'.format(key+':', transformed_value))
    def do_set(self, arg):
        '''Set or toggle a yes/no setting variable in the current session.
    set calculation no  |  set calculation

Available settings are:
  caching: Whether to cache the results of calculations or not.
  calculation: Whether to perform new calculations for values that may be
      outdated in the cache, or just use the possibly outdated cached values.
  continuous_mode: Repeat all analysis and plotting commands until they hit the
      time limit, or forever. Useful if a program is still generating data for
      a directory, but you want to start caching now. (To set a time limit, use
      the command 'set_timelimit'.)
  plotting: Whether to actually output plots, or to just perform and cache the
      calculations for them. Disabling both this and 'calculation' makes most
      analysis and plotting commands do nothing.'''
        args = arg.split()
        varname  = None
        varvalue = None
        if len(args) == 2:
            varname  = args[0]
            varvalue = args[1]
        elif len(args) == 1:
            varname = args[0]
        else:
            print('Incorrect command usage. Try \'help set\'.')
        if varvalue is None:
            try:
                self.settings[varname] = not self.settings[varname]
            except KeyError:
                print('That\'s not a valid setting name. Try '
                      '\'get_settings\'.')
                return
        else:
            value = None
            if str.lower(varvalue) in ('yes','y','t','true'):
                value = True
            elif str.lower(varvalue) in ('no','n','f','false'):
                value = False
            else:
                print('That\'s not a valid setting value. Try \'yes\' or'
                      '\'no\'.')
                return
            if varname in self.settings.keys():
                if varname == 'plotting':
                    self.settings['plotting'] = MPIRANK == 0 and value
                else:
                    self.settings[varname] = value
            else:
                print('That\'s not a valid setting name. Try '
                      '\'get_settings\'.')
                return
        DEBUG                         = self.settings['debug']
        self.data_buffer.calculatingp = self.settings['calculation']
        self.data_buffer.cachingp     = self.settings['caching']
    def complete_set(self, text, line, begidx, endidx):
        position = self.get_arg_position(text, line)
        if position == 1:
            return [i for i in list(self.settings.keys()) if i.startswith(text)]
        elif position == 2:
            return [i for i in ['yes', 'no'] if i.startswith(text)]
    def do_get_timelimit(self, arg):
        '''Print the current time limit set on analysis commands.
    get_timelimit'''
        if self.timelimit == 0:
            print('No timelimit')
        else:
            print(str(self.timelimit)+' seconds')
    def do_set_timelimit(self, arg):
        '''Set a time limit on analysis commands, in seconds. Leave as 0 to let
commands run indefinitely.
    set_timelimit 600'''
        try:
            self.timelimit = int(arg)
        except ValueError:
            print('Enter an integer value of seconds.')

    ## Buffer interaction
    # Data buffer
    def do_clear_data(self, arg):
        '''Clear the data buffer of any data:  clear_data'''
        calculatingp = self.data_buffer.calculatingp
        cachingp     = self.data_buffer.cachingp
        self.data_buffer = FileDataBuffer()
        self.data_buffer.calculatingp = calculatingp
        self.data_buffer.cachingp     = cachingp
    def do_update_caches(self, arg):
        '''Update the caches for the data buffer:  update_caches'''
        self.data_buffer.update_caches(force=True)
    # Text buffer
    def do_clear_text(self, arg):
        '''Clear the text buffer of any text output:  clear_text'''
        self.text_buffer.close()
        self.text_buffer = io.StringIO()
    @single_threaded
    def do_view_text(self, arg):
        '''View the text buffer, less-style:  view_text'''
        subprocess.run(['less'],
                       input=bytes(self.text_buffer.getvalue(), 'utf-8'))
    @single_threaded
    def do_save_text(self, arg):
        parser = argparse.ArgumentParser(
            description='Save the text buffer to a file, optionally specifying'
                        ' a different format.')
        parser.add_argument('path',
                            help='Output path.')
        parser.add_argument('--format',
                            dest='format',
                            nargs='?',
                            default=None,
                            help='Format to save text buffer in.')
        self.do_save_text.__func__.__doc__ = parser.format_help()
        try:
            parsed_args = parser.parse_args(arg.split())
        except SystemExit:
            return
        out_format = None
        if parsed_args.format is None:
            last_segment = parsed_args.path.split('.')[-1]
            if last_segment.lower() in ('tsv','csv'):
                out_format = last_segment.lower()
            else:
                out_format = 'tsv'
        else:
            out_format = parsed_args.format.lower()
        if out_format == 'tsv':
            try:
                open(parsed_args.path, 'w').write(self.text_buffer.getvalue())
            except FileNotFoundError:
                print('Invalid output path.')
        elif out_format == 'csv':
            reader = csv.reader(self.text_buffer, delimiter='\t')
            try:
                with open(parsed_args.path, 'w') as out_file:
                    out_file = csv.writer(out_file)
                    self.text_buffer.seek(0)
                    for row in reader:
                        out_file.writerow(row)
            except FileNotFoundError:
                print('Invalid output path.')
        else:
            print('Invalid output format.')

    # Plot buffer
    def do_clear_plot(self, arg):
        '''Clear the plot buffer:  clear_plot'''
        self.mtpl_buffer.clear()

    ## Basic Rosetta stuff
    @single_threaded
    def do_get_scorefxn(self, arg):
        '''Print the name of the current scorefxn, if any:  get_scorefxn'''
        global PYROSETTA_ENV
        if PYROSETTA_ENV.scorefxn is None:
            print('No scorefxn currently set.')
        else:
            print(PYROSETTA_ENV.scorefxn.get_name())
    def do_set_scorefxn(self, arg):
        '''Set the current scorefxn, optionally applying a patchfile:
    set_scorefxn ref2015  |  set_scorefxn ref2015 docking'''
        global PYROSETTA_ENV
        args = arg.split()
        name = None
        patch = None
        if len(args) == 2:
            name = args[0]
            patch = args[1]
        elif len(args) == 1:
            name = args[0]
        else:
            print('Incorrect command usage. Try \'help set_scorefxn\'')
        if name.startswith('talaris'):
            print('Setting the scorefxn to a talaris flavor crashes Rosetta. '
                  'Don\'t do that.')
            return
        PYROSETTA_ENV.set_scorefxn(name=name, patch=patch)
    def complete_set_scorefxn(self, text, line, begidx, endidx):
        scorefxn_list = get_filenames_from_dir_with_extension(
            '$HOME/.local/lib/python3.5/site-packages/pyrosetta*/pyrosetta/'
            'database/scoring/weights/',
            '.wts', strip_extensions_p = True)
        patches_list = get_filenames_from_dir_with_extension(
            '$HOME/.local/lib/python3.5/site-packages/pyrosetta*/pyrosetta/'
            'database/scoring/weights/',
            '.wts_patch', strip_extensions_p = True)
        position = self.get_arg_position(text, line)
        if position == 1 :
            return [i for i in scorefxn_list if i.startswith(text)]
        elif position == 2:
            return [i for i in patches_list if i.startswith(text)]

    ## Matplotlib stuff
    # fundamental stuff
    @single_threaded
    @pure_plotting
    def do_save_plot(self, arg):
        '''Save the plot currently in the plot buffer:  save_plot name.eps'''
        # plotting should already be off, but it doesn't hurt to double-check
        if MPIRANK == 0:
            if arg:
                extension = arg.split('.')[-1].lower()
                if extension in ('png', 'pdf', 'ps', 'eps', 'svg'):
                    try:
                        self.mtpl_buffer.save_static(arg, format=extension)
                    except Exception as e:
                        print('I tried to save your plot as a static image, '
                              'but I encountered this error:')
                        traceback.print_exc()
                        print('If you didn\'t intend for the saved plot to be '
                              'a static image, remember that files will be '
                              'saved as static images if they have any of the '
                              'following extensions: .png, .pdf, .ps, .eps, or '
                              '.svg.')
                else:
                    try:
                        self.mtpl_buffer.save_animation(arg)
                    except Exception as e:
                        print('I tried to save your plot as an animation, but '
                              'I encountered this error:')
                        traceback.print_exc()
                        print('If you didn\'t intend for the saved plot to be '
                              'an animation, remember that the only supported '
                              'static formats are .png, .pdf, .ps, .eps, and '
                              '.svg.')
            else:
                print('Your plot needs a name.')
    @pure_plotting
    def do_canvas_size(self, arg):
        '''Set the canvas size in inches, giving the width first, then the
height:
    canvas_size 10 10'''
        try:
            val = tuple(float(x) for x in arg.split())
            self.mtpl_buffer.canvas_size = val
            assert len(val) == 2
            assert val[0] > 0 and val[1] > 0
        except (ValueError, AssertionError):
            print('Provide two numbers separated by spaces.')
    # titles and labels
    @pure_plotting
    def do_plot_title(self, arg):
        '''Set the title of the current plot:  plot_title My title'''
        self.mtpl_buffer.current_plot.title = arg
    @pure_plotting
    def do_plot_xlabel(self, arg):
        '''Set the x axis label of the current plot:  plot_xlabel My xlabel'''
        self.mtpl_buffer.current_plot.xlabel = arg
    @pure_plotting
    def do_plot_ylabel(self, arg):
        '''Set the y axis label of the current plot:  plot_ylabel My ylabel'''
        self.mtpl_buffer.current_plot.ylabel = arg
    @pure_plotting
    def do_xticks(self, arg):
        '''Set the xticks on your plot, optionally specifying location.
    xticks 'label 1' 'label 2' 'label 3' |
    xticks label\ 1  label\ 2  label\ 3  |
    xticks ('label 1', 0.1), ('label 2', 0.2), ('label 3', 0.3)'''
        try:
            tick_labels, tick_indices = process_ticks(arg)
        except ValidationError:
            print('Malformed input. See examples in help.')
            return
        current_plot = self.mtpl_buffer.current_plot
        current_plot.xtick_labels = tick_labels
        if tick_indices:
            current_plot.xtick_indices  = tick_indices
    @pure_plotting
    def do_yticks(self, arg):
        '''Set the xticks on your plot, optionally specifying location.
    yticks 'label 1' 'label 2' 'label 3' |
    yticks label\ 1  label\ 2  label\ 3  |
    yticks ('label 1', 0.1), ('label 2', 0.2), ('label 3', 0.3)'''
        try:
            tick_labels, tick_indices = process_ticks(arg)
        except ValidationError:
            print('Malformed input. See examples in help.')
            return
        current_plot = self.mtpl_buffer.current_plot
        current_plot.ytick_labels = tick_labels
        if tick_indices:
            current_plot.ytick_indices  = tick_indices
    @pure_plotting
    def do_set_xticks_rotation(self, arg):
        '''Set the rotation of the xtick labels in your plot.
    set_xticks_rotation 90'''
        try:
            if arg not in ('horizontal', 'vertical'):
                arg = float(arg)
        except ValueError:
            print('Invalid rotation value. It should be "horizontal", '
                  '"vertical", or a number in degrees.')
            return
        self.mtpl_buffer.current_plot.xtick_rotation = arg
    @pure_plotting
    def do_set_yticks_rotation(self, arg):
        '''Set the rotation of the ytick labels in your plot.
    set_yticks_rotation 90'''
        try:
            if arg not in ('horizontal', 'vertical'):
                arg = float(arg)
        except ValueError:
            print('Invalid rotation value. It should be "horizontal", '
                  '"vertical", or a number.')
            return
        self.mtpl_buffer.current_plot.ytick_rotation = arg
    @pure_plotting
    def do_prune_xticks(self, arg):
        '''Remove every other xtick in a subgrid:  prune_xticks (0, 0) (1, 1)'''
        try:
            subgrid = process_subgrid(arg)
        except ValidationError:
            print('Provide two coordinates externally delimited by parentheses '
                  'and internally delimited by commas. See help for an example.')
        current_plot = self.mtpl_buffer.current_plot
        if current_plot.xtick_indices is not None:
            current_plot.xtick_indices = current_plot.xtick_indices[1:-1:2]
        else:
            def f(mtpl_buffer, subgrid):
                ax = mtpl_buffer.get_axes_from_subgrid(subgrid)
                ax.set_xticks(ax.get_xticks()[1:-1:2])
                ax.set_xticklabels(ax.get_xticklabels()[1:-1:2])
            self.mtpl_buffer.add_post_command(f, self.mtpl_buffer, subgrid)
        if current_plot.xtick_labels is not None:
            current_plot.xtick_labels  = current_plot.xtick_labels[1:-1:2]
    # axes stuff
    @pure_plotting
    def do_xlim(self, arg):
        '''Set limits for the x axis.
    xlim 0 1  |  xlim SAME 1'''
        try:
            left, right = arg.split()
            left  = None if left.lower()  == 'same' else float(left)
            right = None if right.lower() == 'same' else float(right)
            self.mtpl_buffer.current_plot.xlim = (left, right)
        except ValueError:
            print('Specify a value for each side of the limits. If you want to'
                  'leave it the same, write "SAME".')
    @pure_plotting
    def do_ylim(self, arg):
        '''Set limits for the y axis.
    ylim 0 1  |  ylim SAME 1'''
        try:
            left, right = arg.split()
            left  = None if left.lower()  == 'same' else float(left)
            right = None if right.lower() == 'same' else float(right)
            self.mtpl_buffer.current_plot.ylim = (left, right)
        except ValueError:
            print('Specify a value for each side of the limits. If you want to'
                  'leave it the same, write "SAME".')
    @pure_plotting
    def do_invert_xaxis(self, arg):
        '''Invert the current axes' x axis:  invert_xaxis'''
        self.mtpl_buffer.current_plot.xlim = tuple(reversed(self.mtpl_buffer \
                                                                .current_plot \
                                                                .xlim))
    @pure_plotting
    def do_invert_yaxis(self, arg):
        '''Invert the current axes' y axis:  invert_yaxis'''
        self.mtpl_buffer.current_plot.ylim = tuple(reversed(self.mtpl_buffer \
                                                                .current_plot \
                                                                .ylim))
    # gridspec stuff
    @pure_plotting
    def do_grid_size(self, arg):
        '''Set the numbers of rows and columns in the grid to which plots are
aligned, if you are including multiple plots. Set the current subgrid with the
command `subgrid`.
    grid_size 2 2'''
        try:
            val = tuple(int(x) for x in arg.split())
            assert len(val) == 2
            assert val[0] > 0 and val[1] > 0
            self.mtpl_buffer.grid_size = val
        except (ValueError, AssertionError):
            print('Provide two positive integers separated by spaces.')
    @pure_plotting
    def do_subgrid(self, arg):
        '''Set the subgrid selected for the current plot. This is specified as
two sets of coordinates, which "slice out" the grid location you want,
increasing from the top left corner; e.g., (0,0) and (1,1) will slice out a 1x1
box from the top left corner of the grid.
    subgrid (0, 0) (1, 1)'''
        try:
            self.mtpl_buffer.subgrid = process_subgrid(arg)
        except ValidationError:
            print('Provide two coordinates externally delimited by parentheses '
                  'and internally delimited by commas. See help for an example.')
    @pure_plotting
    def do_tight_layout(self, arg):
        '''Adjust subplot spacing so that there's no overlaps between
different subplots.
    tight_layout'''
        def f():
            plt.tight_layout()
        self.mtpl_buffer.add_post_command(f)
    @pure_plotting
    def do_add_colorbar(self, arg):
        '''Add a colorbar to the left or right of your figure, for a specified
subgrid. Don't call tight_layout after this; that breaks everything.
    add_colorbar (0, 0) (1, 1) left'''
        side_matcher = re.compile(r'(.*?)(|left|right)')
        matches = side_matcher.fullmatch(arg)
        subgrid_text, side = matches.group(1), matches.group(2) or 'right'
        try:
            subgrid = process_subgrid(subgrid_text)
        except ValidationError:
            print('Provide two coordinates externally delimited by parentheses '
                  'and internally delimited by commas. See help for an example.')
            return
        self.do_tight_layout('')
        def f(mtpl_buffer, subgrid):
            SPACE   = 0.1
            PADDING = 0.6
            fig = plt.gcf()
            w,h = fig.get_size_inches()
            fig.set_size_inches((w/(1-SPACE), h))
            if side == 'right':
                fig.subplots_adjust(right=1-SPACE)
                cbax = fig.add_axes([1-SPACE*(1-PADDING/2), 0.15,
                                     SPACE*(1-PADDING),     0.7])
            elif side == 'left':
                fig.subplots_adjust(left=SPACE)
                cbax = fig.add_axes([SPACE*PADDING/2,   0.15,
                                     SPACE*(1-PADDING), 0.7])
            # Use the given subgrid to extract the last AxesImage artist from
            # the corresponding axes, so that it can be used for the colorbar
            # (the colorbar method requires it because it contains the color
            #  scale). Note that, when running PAPERCLIP, if the user gives an
            # image plotting command, then calls this, then gives another image
            # plotting command on the same subgrid, then the colorbar will be
            # for the second one, not the first. This is arguably somewhat
            # unexpected behavior, but I'm gonna say it's Intended; if you want
            # a colorbar different from the image that shows up in the axes,
            # write a colorbar method that does that.
            imax = mtpl_buffer.get_axes_from_subgrid(subgrid)
            last_im = next(artist for artist in reversed(imax.get_children()) \
                           if isinstance(artist, matplotlib.image.AxesImage))
            fig.colorbar(last_im, cax=cbax)
        self.mtpl_buffer.add_post_command(f, self.mtpl_buffer, subgrid)
    # Plots
    @single_threaded
    @pure_plotting
    def do_add_frame_indicator(self, arg):
        '''Add a red vertical line to the current plot, whose x-axis should be
        the current animation frame. The line's x-position will always be the
        current frame number.'''
        def init(ax, env, drawables):
            drawables['lines'] = ax.axvline(x=0, color='red')
        def update(ax, frame, env, drawables):
            xlim, xmargin = ax.get_xlim(), ax.margins()[0]
            width = xlim[1] - xlim[0]
            if frame <= xlim[1] - width*5/110 + 0.1: # don't ask
                data = drawables['lines'].get_data()
                drawables['lines'].set_data(([frame, frame], data[1]))
                return False
            else:
                return True
        self.mtpl_buffer.add_animation(init, update)
    ## Calculations stuff
    # Text
    @continuous
    def do_table_dir_rmsd_vs_score(self, arg):
        global PYROSETTA_ENV
        parser = argparse.ArgumentParser(
            description='Add a table of RMSDs vs a particular file and scores '
                        'for a directory of PDBs to the text buffer.')
        parser.add_argument('in_dir',
                            action='store',
                            help='The directory to plot for. You may specify a'
                                 ' constraints file for its scoring by adding '
                                 'a colon followed by the path to the file to '
                                 'the end of this path.')
        parser.add_argument('in_file',
                            action='store',
                            help='The PDB to compare with for RMSD.')
        parser.add_argument('--params',
                            dest='params',
                            action='store',
                            nargs='*',
                            default=None,
                            help='Params files for the PDBs.')
        parser.add_argument('--rmsdlim',
                            nargs='*',
                            help='Limits on RMSD for the plot. If a side\'s '
                                 'limit is given as NONE, that side is '
                                 'unlimited.')
        parser.add_argument('--scorelim',
                            nargs='*',
                            help='Limits on score for the plot. If a side\'s '
                                 'limit is given as NONE, that side is '
                                 'unlimited.')
        parser.add_argument('--sorting',
                            dest='sorting',
                            action='store',
                            default='scoreinc',
                            help='Sorting criterion. Can be rmsddec, rmsdinc, '
                                 'scoredec, or scoreinc.')
        self.do_table_dir_rmsd_vs_score.__func__.__doc__ = parser.format_help()
        try:
            parsed_args = parser.parse_args(shlex.split(arg))
        except SystemExit:
            return
        in_dir, cst_path = self.split_cst_path(parsed_args.in_dir)
        filenames_in_in_dir = os.listdir(in_dir)
        filenames_in_in_dir = [filename \
                               for filename in filenames_in_in_dir \
                               if filename.endswith('.pdb')]
        params = process_params(parsed_args.params)
        data = zip(filenames_in_in_dir,
                   self.data_buffer.gather_rmsd(
                       (os.path.join(in_dir, filename) \
                        for filename in filenames_in_in_dir),
                       parsed_args.in_file,
                       'all_atom_rmsd',
                       params=params),
                   self.data_buffer.gather_score(
                       (os.path.join(in_dir, filename) \
                        for filename in filenames_in_in_dir),
                       params=params, cst_path=cst_path))
        # sorting criterion
        criterion = None
        if parsed_args.sorting.lower() == 'rmsddec':
            criterion = lambda p: -p[1]
        elif parsed_args.sorting.lower() == 'rmsdinc':
            criterion = lambda p: p[1]
        elif parsed_args.sorting.lower() == 'scoredec':
            criterion = lambda p: -p[2]
        elif parsed_args.sorting.lower() == 'scoreinc':
            criterion = lambda p: p[2]
        else:
            print('Invalid sorting type.')
            return
        # bounds
        rmsdlowbound  = None
        rmsdupbound   = None
        scorelowbound = None
        scoreupbound  = 0
        if parsed_args.rmsdlim is not None:
            try:
                rmsdlowbound, rmsdupbound = process_limits(parsed_args.rmsdlim)
            except:
                print('Incorrectly specified RMSD limits.')
                return
        if parsed_args.scorelim is not None:
            try:
                scorelowbound, scoreupbound = \
                    process_limits(parsed_args.scorelim)
            except:
                print('Incorrectly specified score limits.')
                return
        self.text_buffer.seek(0, io.SEEK_END)
        writer = csv.writer(self.text_buffer, delimiter='\t')
        # I'm so sorry.
        writer.writerows(sorted([datapoint \
                                 for datapoint in data \
                                 if (((rmsdlowbound is None) or \
                                      rmsdlowbound < datapoint[1]) and \
                                     ((rmsdupbound is None) or \
                                      datapoint[1] < rmsdupbound) and \
                                     ((scorelowbound is None) or \
                                      scorelowbound < datapoint[2]) and \
                                     ((scoreupbound is None) or \
                                      datapoint[2] < scoreupbound))],
                                key=criterion))
    # Plots
    @continuous
    def do_plot_dir_rmsd_vs_score(self, arg):
        global PYROSETTA_ENV
        parser = argparse.ArgumentParser(
            description='For each PDB in a directory, plot the RMSDs vs a '
                        'particular file against their energy score.')
        parser.add_argument('in_dir',
                            action='store',
                            help='The directory to plot for.')
        parser.add_argument('in_file',
                            action='store',
                            help='The PDB to compare with for RMSD.')
        parser.add_argument('--params',
                            dest='params',
                            action='store',
                            nargs='*',
                            default=None,
                            help='Params files for the PDBs.')
        parser.add_argument('--rmsdlim',
                            nargs='*',
                            help='Limits on RMSD for the plot. If a side\'s '
                                 'limit is given as NONE, that side is '
                                 'unlimited.')
        parser.add_argument('--scorelim',
                            nargs='*',
                            help='Limits on score for the plot. If a side\'s '
                                 'limit is given as NONE, that side is '
                                 'unlimited.')
        parser.add_argument('--style',
                            dest='style',
                            action='store',
                            default='ro',
                            help='Matlab-type style to plot points with, like '
                                 '\'ro\' or \'b-\'.')
        self.do_plot_dir_rmsd_vs_score.__func__.__doc__ = parser.format_help()
        try:
            parsed_args = parser.parse_args(shlex.split(arg))
        except SystemExit:
            return
        in_dir, cst_path = self.split_cst_path(parsed_args.in_dir)
        filenames_in_in_dir = os.listdir(in_dir)
        params = process_params(parsed_args.params)
        data = set(zip(self.data_buffer.gather_rmsd(
                           (os.path.join(in_dir, filename) \
                            for filename in filenames_in_in_dir \
                            if filename.endswith('.pdb')),
                           parsed_args.in_file,
                           'all_atom_rmsd',
                           params=params),
                       self.data_buffer.gather_score(
                           (os.path.join(in_dir, filename) \
                            for filename in filenames_in_in_dir \
                            if filename.endswith('.pdb')),
                           params=params, cst_path=cst_path)))
        rmsdlowbound  = None
        rmsdupbound   = None
        scorelowbound = None
        scoreupbound  = 0
        if parsed_args.rmsdlim is not None:
            try:
                rmsdlowbound, rmsdupbound = process_limits(parsed_args.rmsdlim)
            except:
                print('Incorrectly specified RMSD limits.')
                return
        if parsed_args.scorelim is not None:
            try:
                scorelowbound, scoreupbound = \
                    process_limits(parsed_args.scorelim)
            except:
                print('Incorrectly specified score limits.')
                return
        # I'm so sorry.
        rmsds, scores = zip(*(datapoint \
                              for datapoint in data \
                              if (((rmsdlowbound is None) or \
                                   rmsdlowbound < datapoint[0]) and \
                                  ((rmsdupbound is None) or \
                                   datapoint[0] < rmsdupbound) and \
                                  ((scorelowbound is None) or \
                                   scorelowbound < datapoint[1]) and \
                                  ((scoreupbound is None) or \
                                   datapoint[1] < scoreupbound))))
        if self.settings['plotting']:
            self.mtpl_buffer.add_plot('plot', rmsds, scores, parsed_args.style)
    @continuous
    def do_plot_dir_neighbors(self, arg):
        parser = argparse.ArgumentParser(
            description='Make a heatmap of how often two residues neighbor '
                        'each other in a given directory. You must specify '
                        'the range of residues over which to create the '
                        'heatmap.')
        parser.add_argument('in_dir',
                            action='store')
        parser.add_argument('start_i',
                            type=int,
                            action='store')
        parser.add_argument('end_i',
                            type=int,
                            action='store')
        parser.add_argument('--params',
                            dest='params',
                            action='store',
                            nargs='*',
                            default=None)
        self.do_plot_dir_neighbors.__func__.__doc__ = parser.format_help()
        try:
            parsed_args = parser.parse_args(arg.split())
        except SystemExit:
            return
        params = process_params(parsed_args.params)
        filenames_in_in_dir = os.listdir(parsed_args.in_dir)
        results = [np.array(result)
                   for result in \
                       self.data_buffer.gather_neighbors(
                           (os.path.join(parsed_args.in_dir, filename) \
                            for filename in filenames_in_in_dir \
                            if filename.endswith('.pdb')),
                           params=params)]
        if self.settings['plotting']:
            avg_matrix = np.mean(np.stack(results), axis=0)
            self.mtpl_buffer.add_plot(
                'imshow', avg_matrix, cmap='jet', interpolation='nearest',
                extent=[parsed_args.start_i, parsed_args.end_i,
                        parsed_args.end_i, parsed_args.start_i],
                aspect=1, vmin=0, vmax=1)
            self.mtpl_buffer.current_plot.gridp = False
            self.mtpl_buffer.current_plot.tick_params = \
                {'axis':'both', 'which':'both',
                 'top':False, 'bottom':False, 'left':False, 'right':False}
    @continuous
    def do_plot_neighbors_bar(self, arg):
        parser = argparse.ArgumentParser(
            description='Create a bar chart of a set of dirs for the average '
                        'long-distance neighbor rate among the PDBs in those '
                        'dirs. Set the labels using xticks.')
        parser.add_argument('dirs',
                            nargs='*',
                            help='Directories to chart.')
        parser.add_argument('--params',
                            nargs='*',
                            help='Path to params files; it is assumed that '
                                 'all PDBs need the same params. (If they '
                                 'don\'t, you can just list the params '
                                 'required by all of the PDBs together; '
                                 'Rosetta doesn\'t care.)')
        self.do_plot_neighbors_bar.__func__.__doc__ = parser.format_help()
        try:
            parsed_args = parser.parse_args(shlex.split(arg))
        except SystemExit:
            return
        params = process_params(parsed_args.params)
        values = []
        try:
            for pdbdir in parsed_args.dirs:
                filenames = os.listdir(pdbdir)
                results = [np.array(result) \
                           for result in \
                               self.data_buffer.gather_neighbors(
                                   (os.path.join(pdbdir, filename) \
                                    for filename in filenames \
                                    if filename.endswith('.pdb')),
                                   params=params)]
                avg_matrix = np.mean(np.stack(results), axis=0)
                # get a horizontally stacked version of matrix with middle
                # five diagonals removed
                N_REMOVED_DIAG = 5 # number of removed diagonals; must be odd
                stacked = np.hstack(
                    [np.hstack(avg_matrix[i][i+N_REMOVED_DIAG//2+1:] \
                               for i in range(avg_matrix.shape[0])),
                     np.hstack(avg_matrix[i][:max(i-N_REMOVED_DIAG//2,0)] \
                               for i in range(avg_matrix.shape[0]))])
                values.append(np.mean(stacked))
        except:
            print('Something weird went wrong; that\'s probably not a valid '
                  'set of dirs.')
            traceback.print_exc()
        if self.settings['plotting']:
            self.mtpl_buffer.add_plot('barh', np.arange(len(values)), values,
                                      align='center')
    @continuous
    def do_plot_neighbors_comparison_bar(self, arg):
        parser = argparse.ArgumentParser(
            description='Plot a grouped bar chart where each group corresponds'
                        ' to a group of compared directories for different '
                        'PDBs, with each bar corresponding to a different PDB.'
                        ' The comparison itself is fraction nonnative '
                        'long-distance neighbors, with nativeness determined '
                        'by the presence of the contacts in a particular PDB '
                        'you specify (for each PDB in a directory group). Set '
                        'the labels using xticks.')
        parser.add_argument('dirs',
                            nargs='*',
                            help='Directories to chart. Give the directories '
                                 'in the same order their bars will appear in '
                                 'on the chart. So if you were comparing dirs '
                                 'for PDB1 and PDB2, you\'d list the dirs in '
                                 'the order "PDB1-dir1 PDB2-dir1 PDB1-dir2 '
                                 'PDB2-dir2." You may additionally specify a '
                                 'constraints file for each dir by joining it '
                                 'to the end of the path with a colon.')
        parser.add_argument('--pdbs',
                            dest='pdbs',
                            action='store',
                            nargs='*',
                            help='Idealized PDBs to compare dirs against.')
        parser.add_argument('--params',
                            dest='params',
                            action='store',
                            nargs='*',
                            help='Path to params files; it is assumed that '
                                 'all PDBs need the same params. (If they '
                                 'don\'t, you can just list the params '
                                 'required by all of the PDBs together; '
                                 'Rosetta doesn\'t care.)')
        parser.add_argument('--scorelim',
                            nargs='*',
                            help='Limits on score for the plot. If a side\'s '
                                 'limit is given as NONE, that side is '
                                 'unlimited.')
        parser.add_argument('--colors',
                            dest='colors',
                            default=None,
                            help='List of Matlab-type colors to use for bars, '
                                 'as a smooshed-together list of letters. '
                                 'Example:  rgbcmyk')
        parser.add_argument('--missing-only',
                            dest='missingp',
                            action='store_true',
                            help='Catalog fraction of nonnative contacts only '
                                 'for contacts present in prototypes.')
        parser.add_argument('--nonnative',
                            dest='nonnativep',
                            action='store_true',
                            help='Catalog fraction nonnative contacts instead '
                                 'of native contacts.')
        parser.add_argument('--show-errors',
                            dest='errorsp',
                            action='store_true',
                            help='Add error bars (1 sigma).')
        self.do_plot_neighbors_comparison_bar \
            .__func__.__doc__ = parser.format_help()
        try:
            parsed_args = parser.parse_args(shlex.split(arg))
        except SystemExit:
            return
        params = process_params(parsed_args.params)
        npdbs = len(parsed_args.pdbs)
        if len(parsed_args.dirs) % npdbs != 0:
            print('Incorrect dirs spec; number of dirs must be multiple of '
                  'number of pdbs. Try "help plot_neighbors_comparison_bar".')
            return
        pdbmatrices = [np.array(result) \
                       for result in self.data_buffer.gather_neighbors(
                                         parsed_args.pdbs,
                                         params=params)]
        # parse bounds
        scorelowbound = None
        scoreupbound  = 0
        if parsed_args.scorelim is not None:
            try:
                scorelowbound, scoreupbound = \
                    process_limits(parsed_args.scorelim)
            except:
                print('Incorrectly specified score limits.')
                return
        # do the calculations
        values = []
        if parsed_args.errorsp:
            errors = []
        else:
            errors = None
        dirs_and_csts = [self.split_cst_path(d) for d in parsed_args.dirs]
        try:
            for index, pdbdir_and_cst in enumerate(dirs_and_csts):
                pdbdir, cst_path = pdbdir_and_cst
                prototype = pdbmatrices[index % npdbs]
                filenames = os.listdir(pdbdir)
                scores = None
                if scorelowbound is not None or scoreupbound is not None:
                    scores = self.data_buffer.gather_score(
                                 (os.path.join(pdbdir, filename) \
                                  for filename in filenames \
                                  if filename.endswith('.pdb')),
                                 params=params, cst_path=cst_path)
                results = [(np.array(result) - prototype)**2 \
                           for result in \
                               self.data_buffer.gather_neighbors(
                                   (os.path.join(pdbdir, filename) \
                                    for filename in filenames \
                                    if filename.endswith('.pdb')),
                                   params=params)]
                results = [m for i,m in enumerate(results) \
                           if (((scorelowbound is None) or \
                                scorelowbound < scores[i]) and \
                               ((scoreupbound is None) or \
                                scores[i] < scoreupbound))]
                # get horizontally stacked versions of matrices with middle
                # five diagonals removed
                N_REMOVED_DIAG = 5 # number of removed diagonals; must be odd
                linresults = [remove_ndarray_diagonals(m, N_REMOVED_DIAG) \
                              for m in results]
                fracs = None
                if parsed_args.missingp:
                    linprototype = remove_ndarray_diagonals(prototype,
                                                            N_REMOVED_DIAG)
                    fracs = [np.sum(m*linprototype)/np.sum(linprototype) \
                             for m in linresults]
                else:
                    fracs = [np.sum(m)/m.size for m in linresults]
                if not parsed_args.nonnativep:
                    fracs = [1-m for m in fracs]
                values.append(np.mean(fracs))
                if parsed_args.errorsp:
                    errors.append(np.std(fracs))
        except:
            print('Something weird went wrong.')
            traceback.print_exc()
        if self.settings['plotting']:
            nbins = len(parsed_args.dirs) // len(parsed_args.pdbs)
            width = 0.5/nbins
            indices = np.array(
                          tuple(
                              itertools.chain.from_iterable(
                                  ((i+(j-npdbs/2)*width \
                                    for j in range(npdbs)) \
                                   for i in range(nbins)))))
            self.mtpl_buffer.add_plot('bar', indices, values, yerr=errors,
                                      color=parsed_args.colors, ecolor='k',
                                      width=width)
    @single_threaded
    def do_plot_traj_rmsd(self, arg):
        parser = argparse.ArgumentParser(
            description = 'Plots the total RMSD of a trajectory over time, '
                          'with time measured in frames and RMSD measured in '
                          'angstroms.')
        parser.add_argument('mdcrd_path',
                            action='store')
        parser.add_argument('prmtop_path',
                            action='store')
        parser.add_argument('--reference',
                            action='store',
                            nargs='?',
                            default=None,
                            help='.mdcrd file containing a reference '
                                 'structure. If none is provided, the first '
                                 'frame of the trajectory will be used.')
        self.do_plot_traj_rmsd \
            .__func__.__doc__ = parser.format_help()
        try:
            parsed_args = parser.parse_args(shlex.split(arg))
        except SystemExit:
            return
        try:
            frames, rmsds = zip(*enumerate(
                self.data_buffer.get_traj_rmsd(
                                    parsed_args.mdcrd_path,
                                    parsed_args.prmtop_path,
                                    reference_path=parsed_args.reference)))
        except Exception as e:
            traceback.print_exc()
        self.mtpl_buffer.add_plot('plot', frames, rmsds)
    def do_plot_traj_residue_rmsd_bar(self, arg):
        parser = argparse.ArgumentParser(
            description = 'Plots a bar chart of the RMSDs (measured in '
                          'angstroms) of each residue in a trajectory, '
                          'animating it over time.')
        parser.add_argument('mdcrd_path',
                            action='store')
        parser.add_argument('prmtop_path',
                            action='store')
        parser.add_argument('--reference',
                            action='store',
                            nargs='?',
                            default=None,
                            help='.mdcrd file containing a reference '
                                 'structure. If none is provided, the first '
                                 'frame of the trajectory will be used.')
        parser.add_argument('--time-average',
                            dest='averagep',
                            action='store_true')
        self.do_plot_traj_residue_rmsd_bar \
            .__func__.__doc__ = parser.format_help()
        try:
            parsed_args = parser.parse_args(shlex.split(arg))
        except SystemExit:
            return
        mask = '!:WAT,Na+,Cl-'
        res_nums, res_names = zip(*self.data_buffer \
                                  .get_residues(parsed_args.prmtop_path,
                                                mask=mask))
        res_rmsds = self.data_buffer \
                        .get_traj_residue_rmsd(parsed_args.mdcrd_path,
                                               parsed_args.prmtop_path,
                                               mask=mask,
                                               reference_path= \
                                                   parsed_args.reference)
        def init(ax, env, drawables):
            env['n_residues'] = len(res_nums)
            if parsed_args.averagep:
                env['rmsds'] = bottleneck.move_mean(res_rmsds,
                                                    window=50,
                                                    min_count=1).transpose()
            else:
                env['rmsds'] = res_rmsds.transpose()
            env['pw'].set_plot('bar',
                               range(env['n_residues']),
                               env['rmsds'][0])
            env['pw'].ylim = (0, 1.1*np.amax(env['rmsds']))
            env['pw'].xtick_labels   = range(1, env['n_residues']+1, 2)
            env['pw'].xtick_indices  = range(0, env['n_residues'], 2)
            env['pw'].xtick_rotation = 'vertical'
            env['bars'] = env['pw'].plot(ax)
            for index, bar in enumerate(env['bars']):
                drawables[index] = bar
        def update(ax, frame, env, drawables):
            try:
                for index, rmsd in enumerate(env['rmsds'][frame]):
                    drawables[index].set_height(rmsd)
                return False
            except IndexError:
                return True
        self.mtpl_buffer.add_animation(init, update)
    def do_plot_traj_residue_rmsd_carpet(self, arg):
        parser = argparse.ArgumentParser(
            description = 'Plots a 2D carpet of the RMSDs (measured in '
                          'angstroms) of each residue in a trajectory, with '
                          'the time on the x axis and the residue number on '
                          'the y axis.')
        parser.add_argument('mdcrd_path',
                            action='store')
        parser.add_argument('prmtop_path',
                            action='store')
        parser.add_argument('--reference',
                            action='store',
                            nargs='?',
                            default=None,
                            help='.mdcrd file containing a reference '
                                 'structure. If none is provided, the first '
                                 'frame of the trajectory will be used.')
        self.do_plot_traj_residue_rmsd_carpet \
            .__func__.__doc__ = parser.format_help()
        try:
            parsed_args = parser.parse_args(shlex.split(arg))
        except SystemExit:
            return
        self.mtpl_buffer \
            .add_plot('imshow',
                      self.data_buffer \
                          .get_traj_residue_rmsd(parser.mdcrd_path,
                                                 parser.prmtop_path,
                                                 reference_path= \
                                                     parsed_args.reference))
    @single_threaded
    def do_plot_traj_native_contact_fraction(self, arg):
        parser = argparse.ArgumentParser(
            description = 'Plots the fraction native Ca contacts of a '
                          'trajectory over time, with time measured in frames, '
                          'and the first frame being taken to be fully '
                          '"native".')
        parser.add_argument('mdcrd_path',
                            action='store')
        parser.add_argument('prmtop_path',
                            action='store')
        parser.add_argument('--missing-only',
                            dest='missingp',
                            action='store_true',
                            help='Catalog fraction of nonnative contacts only '
                                 'for contacts present in prototypes.')
        parser.add_argument('--nonnative',
                            dest='nonnativep',
                            action='store_true',
                            help='Catalog fraction nonnative contacts instead '
                                 'of native contacts.')
        self.do_plot_traj_native_contact_fraction \
            .__func__.__doc__ = parser.format_help()
        try:
            parsed_args = parser.parse_args(shlex.split(arg))
        except SystemExit:
            return
        frames, contacts = zip(*enumerate(
            (self.data_buffer.get_traj_pairwise_distances(
                parsed_args.mdcrd_path,
                parsed_args.prmtop_path) < 8).astype(int)))
        prototype = contacts[0]
        # in natives, 0 indicates a native contact or native lack-of-contact,
        # while 1 indicates a nonnative contact or a nonnative lack-of-contact
        natives = [(frame - prototype)**2 for frame in contacts]
        # linearize everything, and remove the center 5 diagonals
        N_REMOVED_DIAG = 5 # number of removed diagonals; must be odd
        linnatives   = [remove_ndarray_diagonals(m, N_REMOVED_DIAG) \
                        for m in natives]
        fracs = None
        if parsed_args.missingp:
            linprototype = remove_ndarray_diagonals(prototype, N_REMOVED_DIAG)
            fracs = [np.sum(m*linprototype)/np.sum(linprototype) \
                     for m in linnatives]
        else:
            fracs = [np.sum(m)/m.size for m in linresults]
        if not parsed_args.nonnativep:
            fracs = [1-m for m in fracs]
        self.mtpl_buffer.add_plot('plot', frames, fracs)
    @single_threaded
    def do_plot_traj_contact_frequency_map(self, arg):
        parser = argparse.ArgumentParser(
            description = 'Plots the per-frame frequency of native contacts '
                          'occurring over a trajectory, with the first frame '
                          'taken to be fully "native".')
        parser.add_argument('mdcrd_path',
                            action='store')
        parser.add_argument('prmtop_path',
                            action='store')
        parser.add_argument('--nonnatives',
                            action='store_true',
                            dest='nonnativesp',
                            help='Records nonnative contact frequency instead '
                                 'of contact frequency in general.')
        parser.add_argument('--reference',
                            action='store',
                            nargs='?',
                            default=None,
                            help='.mdcrd file containing a reference '
                                 'structure. If none is provided, the first '
                                 'frame of the trajectory will be used.')
        self.do_plot_traj_contact_frequency_map \
            .__func__.__doc__ = parser.format_help()
        try:
            parsed_args = parser.parse_args(shlex.split(arg))
        except SystemExit:
            return
        contacts = \
            (self.data_buffer.get_traj_pairwise_distances(
                parsed_args.mdcrd_path,
                parsed_args.prmtop_path) < 8).astype(int)
        if parsed_args.nonnativesp:
            if parsed_args.reference is not None:
                prototype = \
                    (self.data_buffer.get_traj_pairwise_distances(
                        parsed_args.reference,
                        parsed_args.prmtop_path) < 8).astype(int)[0]
            else:
                prototype = contacts[0]
            # 0 indicates a native contact or native lack-of-contact, while 1
            # indicates a nonnative contact or a nonnative lack-of-contact
            frequencies = np.mean(np.array([(frame - prototype)**2 \
                                            for frame in contacts]),
                                  axis=0)
        else:
            frequencies = np.mean(contacts, axis=0)
        self.mtpl_buffer.add_plot('imshow', frequencies)
        self.mtpl_buffer.current_plot.gridp = False
    @single_threaded
    def do_plot_traj_distances(self, arg):
        parser = argparse.ArgumentParser(
            description = 'Plot an animated heatmap of the pairwise atomic '
                          'distances over a trajectory. Plots CA distances '
                          'by default.')
        parser.add_argument('mdcrd_path',
                            action='store')
        parser.add_argument('prmtop_path',
                            action='store')
        parser.add_argument('mask',
                            action='store',
                            nargs='?',
                            default='@CA',
                            help='Alternative atom mask for distances.')
        parser.add_argument('--short',
                            action='store_true',
                            help='Threshold the visual range of the distance '
                                 'to 3-8 angstroms.')
        self.do_plot_traj_distances \
            .__func__.__doc__ = parser.format_help()
        try:
            parsed_args = parser.parse_args(shlex.split(arg))
        except SystemExit:
            return
        dists = self.data_buffer.get_traj_pairwise_distances(
                                     parsed_args.mdcrd_path,
                                     parsed_args.prmtop_path,
                                     mask1=parsed_args.mask,
                                     mask2=parsed_args.mask)
        def init(ax, env, drawables):
            env['dists'] = dists
            if parsed_args.short:
                vmin, vmax = 3, 8
            else:
                vmin, vmax = 0, np.amax(dists)
            env['pw'].set_plot('imshow', env['dists'][0],
                               vmin=vmin, vmax=vmax)
            drawables['image'] = env['pw'].plot(ax)
        def update(ax, frame, env, drawables):
            try:
                drawables['image'].set_data(env['dists'][frame])
                return False
            except IndexError:
                return True
        self.mtpl_buffer.add_animation(init, update)
        self.mtpl_buffer.current_plot.gridp = False
    @single_threaded
    def do_plot_traj_ca_distances_deltas(self, arg):
        parser = argparse.ArgumentParser(
            description = 'Plot an animated heatmap of the differences of the '
                          'pairwise CA distances from the initial frame over a '
                          'trajectory.')
        parser.add_argument('mdcrd_path',
                            action='store')
        parser.add_argument('prmtop_path',
                            action='store')
        self.do_plot_traj_ca_distances_deltas \
            .__func__.__doc__ = parser.format_help()
        try:
            parsed_args = parser.parse_args(shlex.split(arg))
        except SystemExit:
            return
        dists = list(self.data_buffer.get_traj_pairwise_distances(
                                          parsed_args.mdcrd_path,
                                          parsed_args.prmtop_path))
        deltas = [m-dists[0] for m in dists]
        deltas_min, deltas_max = np.amin(deltas), np.amax(deltas)
        absmax = max(abs(deltas_min), abs(deltas_max))
        def init(ax, env, drawables):
            env['deltas'] = deltas
            env['pw'].set_plot('imshow', env['deltas'][0],
                               vmin=-absmax, vmax=absmax)
            drawables['image'] = env['pw'].plot(ax)
        def update(ax, frame, env, drawables):
            try:
                drawables['image'].set_data(env['deltas'][frame])
                return False
            except IndexError:
                return True
        self.mtpl_buffer.add_animation(init, update)
        self.mtpl_buffer.current_plot.gridp = False

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description = 'Interactive command-line interface for plotting and '
                      'analysis of batches of PDB files.')
    parser.add_argument('--background',
                        dest='backgroundp',
                        action='store_true',
                        help='Run given script in background and then '
                             'terminate. If no script is given, just do '
                             'nothing and terminate.')
    parser.add_argument('--continuous',
                        dest='continuousp',
                        action='store_true',
                        help='Re-run caching operations until they hit the '
                             'time limit or forever. By default, suppresses '
                             'plots.')
    parser.add_argument('script',
                        action='store',
                        nargs='?',
                        default=None,
                        help='.cmd file to run before entering interactive '
                             'mode.')
    parsed_args = parser.parse_args()
    PYROSETTA_ENV = PyRosettaEnv()
    OURCMDLINE = OurCmdLine()
    OURCMDLINE.settings['continuous_mode'] = parsed_args.continuousp
    OURCMDLINE.settings['plotting'] = not parsed_args.continuousp
    if MPIRANK != 0:
        DEVNULL = open(os.devnull, 'w')
        sys.stdout = DEVNULL
    if parsed_args.script is not None:
        with open(parsed_args.script, encoding='utf-8') as f:
            for command in f.read().splitlines():
                OURCMDLINE.cmdqueue.append(u'echo ' + command)
                OURCMDLINE.cmdqueue.append(command)
    if parsed_args.backgroundp:
        OURCMDLINE.cmdqueue.extend(['quit'])
    OURCMDLINE.cmdloop()
    if MPIRANK != 0:
        sys.stdout = STDOUT # defined at beginning of file
        DEVNULL.close()
